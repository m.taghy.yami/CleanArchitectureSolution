﻿using Application.Common.Model;

namespace Application.UseCases.Membership.UserCase.Query.GetUserItem
{
    public class UserItemQuery : BaseLoadItemQuery<QueryResponse<UserDto>>
    {
    }
}
